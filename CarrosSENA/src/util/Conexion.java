package util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class Conexion {
	
	private static String bd = "ventacarros";
	private static String url = "jdbc:mysql://localhost:3306/";
	private static String user = "root";
	private static String password = "";
	
	Connection conexion;
	
	public Conexion (){
		
		try {
			Class.forName("com.mysql.jdbc.Driver");
			conexion = DriverManager.getConnection(url+bd, user, password);
			
		} catch (ClassNotFoundException | SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		
	}

	
	public Connection obtenerConexion() {
		
		return conexion;
	}
	
	
	
	public Connection cerrarConexio() throws SQLException {
		
		conexion.close();
		conexion = null;
		return conexion;
	}
	
	public static void main(String[] args) {
		
		new Conexion();
		
	}
	
	
	
	
}



