package util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class MysqlTest {


	public static void main(String[] args) {


		try {
			
			Class.forName("com.mysql.jdbc.Driver");

			String bd = "ventacarros";
			String url = "jdbc:mysql://localhost:3306/";
			String user = "root";
			String password = "";
			String query = "select * from categoria";

			Connection conexion = DriverManager.getConnection(url+bd, user, password);
			Statement st = conexion.createStatement();
			ResultSet rs = st.executeQuery(query);
			
			while (rs.next()) {
				System.out.println(rs.getString(2));
			}
			
			
		} catch (ClassNotFoundException | SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
